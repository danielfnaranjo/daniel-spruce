<?php get_header(); ?>



<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="..." alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src=". .." alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="..." alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<!-- Slider section -->
<section class="section section--lg">
        <div class="container">
            <div class="row justify-content-center text-center">
                <div class="col-md-10 col-lg-7 col-xl-6">
                    <h2 class="h1 text-white">Spruce</h2>
                    <p class="lead text-muted">
                    <?php the_field('slide_title');?>
                    </p>
                    <a href="<?php the_field('slide_button_link')?>" class="btn btn-secondary">Learn more</a>
                </div>
            </div>
        </div>
    </section>




<!-- About US Section -->
<section class="section section--lg">
        <div class="container">
            <div class="row justify-content-center text-center">
                <div class="col-md-10 col-lg-7 col-xl-6">
                    <h2 class="h1 text-dark">
                        <?php the_field('about_title'); ?>
                    </h2>
                    <h3>
                        <?php the_field('about_subtitle'); ?>
                    </h3>
                    <!-- <a href="#" class="btn btn-secondary">Learn more</a> -->
                </div>
            </div>

            <div class="row justify-content-center text-center">
                <div class="col-md-6 col-lg-6 col-xl-6">
                    <ol>
                        <li>Employee Name</li>
                        <li>Employee Name</li>
                        <li>Employee Name</li>
                        <li>....</li>

                    </ol>

                    <p class="text-left">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
Lorem ipsum dolor sit amet, consetetur sadipscing elitr    
                    </p>

                </div>
                <div class="col-md-6 col-lg-6 col-xl-6">
                    <!-- Delete and replace with pictures slider -->
                    <p>Add slider with employees pics</p>

                </div>
            </div>
        </div>
    </section>



    
    <section class="section section--lg bg">
        <div class="container">
            <div class="row justify-content-center text-center">
                <div class="col-md-3 col-lg-3 col-xl-3">
                    <h2>
                        <?php the_field('section_title');?>
                    </h2>
                    <ul>
                        <li>Item</li>
                        <li>Item</li>
                        <li>Item</li>
                        <li>Item</li>
                        <li>Item</li>
                        <li>...</li>
                    </ul>
                </div>
                <div class="col-md-9 col-lg-9 col-xl-9">
                       <!-- Delete and replace with work slider -->
                       <p>Add slider with pics</p>
             </div>
            </div>

            <div class="row justify-content-center text-center ">
                <div class="col-md-10 col-lg-7 col-xl-6">
                    
                </div>
            </div>

            <div class="row text-center section-about">
            <div class="col-md-2 col-lg-4 col-xl-4">
                <div class="quote-element">
                23
                </div></div>    
            <div class="col-md-8 col-lg-6 col-xl-6 text-left">
                <h2>What customers say about us</h2>    
            </div>
            <div class="col-md-2 col-lg-2 col-xl-2">
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>  
                
            <div class="row justify-content-center text-center">
                <div class="col-md-10 col-lg-10 col-xl-10 review py-4">




                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                    
                    <h2>Slider with comments from customers</h2>  
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro quae, dolorum, id, expedita eaque nulla explicabo architecto error quisquam deserunt nesciunt delectus tempora hic labore quasi tempore soluta non eligendi?</p>
                    <b>Customer name</b>  
                    </div>
                    <div class="carousel-item">
                    
                    <h2>Slider with comments from customers</h2>  
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro quae, dolorum, id, expedita eaque nulla explicabo architecto error quisquam deserunt nesciunt delectus tempora hic labore quasi tempore soluta non eligendi?</p>
                    <b>Customer name</b>  
                    </div>
                    <div class="carousel-item">
                    
                    <h2>Slider with comments from customers</h2>  
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro quae, dolorum, id, expedita eaque nulla explicabo architecto error quisquam deserunt nesciunt delectus tempora hic labore quasi tempore soluta non eligendi?</p>
                    <b>Customer name</b>  
                    </div>
                </div>
                
                </div>

                </div>
            </div>
        </div>
                        
        </div>
    </section>
    
    


<?php get_footer();