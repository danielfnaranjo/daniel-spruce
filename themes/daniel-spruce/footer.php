<footer class="bg-secondary">

    <div class="container spruce-footer py-2">
        <div class="row py-4 my-5">
            <div class="col-4">
                <h2><?php the_field('footer_title')?></h2>
                <h3><?php the_field('footer_description')?></h3>
                <a class="btn btn-outline-custom" href="#">Contact Us</a>
            </div>        
            <div class="col-8">
                
            </div>
            </div>

            <div class="container spruce-copyright">
                <div class="row py-4">
                    <div class="col-12 col-lg-4 text-white text-center"><p>&copy; <?php echo Date('Y').' '.get_bloginfo('name');?></p></div>
                <div class="col-12 col-lg-4 text-white text-center"><p><a href="https://sproing.ca" target="_blank">Terms & Conditions | Privacy Policy </a></p></div>
                <div class="col-12 col-lg-4 text-white text-center"><p>Designed, Developed and Hosted by <a href="https://sproing.ca" target="_blank">Sproing Creative</a></p></div>
            </div>
            </div>
        </div>
        </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>