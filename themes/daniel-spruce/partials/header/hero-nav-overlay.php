<nav class="navbar navbar-expand-md navbar-fixed-top main-nav">
    <div class="container">
        <ul class="nav navbar-nav mx-auto">
            <li class="nav-item">
                <a class="nav-logo" href="<?php echo esc_url(home_url('/')); ?>">
                    <img src="<?php bloginfo('template_url'); ?>/images/logo.svg" alt="<?php bloginfo('name'); ?> - Logo" class="img-fluid">
                </a>
            </li>
        </ul>
        <ul class="nav navbar-nav">
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">###</a>
                <?php wp_nav_menu([
            'theme_location' => 'primary',
           
            'fallback_cb' => '',
            'menu_id' => 'main-menu',
            
        ]); ?>
            </li>
        </ul>
    </div>
</nav>